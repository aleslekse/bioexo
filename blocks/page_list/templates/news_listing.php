<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$dh = Loader::helper('date'); //<--uncomment this line if displaying dates (see below)
$imgHelper = Loader::Helper('image');

?>

<div class="news-listing">
	<?php foreach ($pages as $page): 

		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$description = $page->getCollectionDescription();
		$description = $controller->truncateSummaries ? $th->shorten($description, $controller->truncateChars) : $description;
		$description = $th->entities($description);	
		$month = $page->getCollectionDatePublic('M');
		$day = $page->getCollectionDatePublic('d');
	?>
	<a class="item clearfix" href="<?php  echo $url ?>">
		<div class="date-box">
			<div><em><strong>
				<span class="month"><?= $month ?></span>
				<span class="day"><?= $day ?></span>
			</strong></em></div>	
		</div>
		<h2><?php  echo $title ?></h2>
		<div class="summary">
			<?= $description ?>
		</div>
	</a>	
	<?php endforeach; ?>
</div>
<?php

if ($paginate && $num > 0 && is_object($pl) && ($summary = $pl->getSummary()) && $summary->pages > 1): 
	?><div id="pagination"><?php  echo $paginator->getPages(); ?></div><?php
endif;

