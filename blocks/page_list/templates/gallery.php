<?php

defined('C5_EXECUTE') or die("Access Denied.");

$galleryTypes = $controller->fetchFilterValues('gallery_type');
$years = $controller->fetchFilterValues('year');
foreach ($years as $k=>$year) {
	$years[$k] = (int)$year;
}

$currentYear = $_GET['year'];
$currentType = $_GET['gallery_type'];

$db= Loader::db();

?>

<form class="gallery-filter" method="GET">
	<select name="year">
		<option value="">Vsa leta</option>
		<? foreach($years as $year): ?>
		<option value="<?= $year; ?>"<? if ($currentYear==$year) echo ' selected'; ?>><?= $year; ?></option>
		<? endforeach; ?>
	</select>
	<select name="gallery_type">
		<option value="">Vse zvrsti</option>
		<? foreach($galleryTypes as $type): ?>
		<option value="<?= $type; ?>"<? if ($currentType==$type) echo ' selected'; ?>><?= $type; ?></option>
		<? endforeach; ?>
	</select>
	<button type="submit">Prikaži</button>
</form>


<?php 
$rssUrl = $showRss ? $controller->getRssUrl($b) : '';
$th = Loader::helper('text');
$imgHelper = Loader::helper('image');


//$ih = Loader::helper('image'); //<--uncomment this line if displaying image attributes (see below)
//$dh = Loader::helper('date'); //<--uncomment this line if displaying dates (see below)
//Note that $nh (navigation helper) is already loaded for us by the controller (for legacy reasons)
?>
<?php foreach (array_chunk($pages, 4) as $chunk): ?>
<div class="row gallery-list">
	<?php  foreach ($chunk as $page):
		// Prepare data for each page being listed...
		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$target = empty($target) ? '_self' : $target;
		$description = $page->getCollectionDescription();
		$description = $controller->truncateSummaries ? $th->shorten($description, $controller->truncateChars) : $description;
		$description = $th->entities($description);	
		$year = $page->getAttribute('year');
		
		$image = false;
		$count = 0;
		$blocks = $page->GetBlocks();
		foreach ($blocks as $block) {
			if ($block->getBlockTypeHandle()=='sortable_fancybox_gallery') {
				$fsId = $db->GetOne("select fsId from btSortableFancyboxGallery where bID=?", $block->getBlockID());
				$files = FileSet::getFilesBySetID($fsId);
				$count += sizeof($files);
				if ($files && !$image) {
					$image = $files[0];
				}
				break;
			}
		}

		?>
		<div class="span1">
			<a href="<?php  echo $url ?>" target="<?php  echo $target ?>" class="box"> 
				<span class="year"><?= $year ?></span>
				<span class="count"><?= $count ?> slik</span>
				<h3><?= $title ?></h3>
				<?php 
					if ($image):
						$imgHelper->outputThumbnail($image, 235, 175, '', false, true);
					endif; 
				?>	
			</a>
		</div>
	<?php  endforeach; ?>
</div>	
<?php endforeach; ?>


<?php  if ($showPagination): ?>
	<div id="pagination">
		<div class="ccm-spacer"></div>
		<div class="ccm-pagination">
			<span class="ccm-page-left"><?php  echo $paginator->getPrevious('&laquo; ' . t('Previous')) ?></span>
			<?php  echo $paginator->getPages() ?>
			<span class="ccm-page-right"><?php  echo $paginator->getNext(t('Next') . ' &raquo;') ?></span>
		</div>
	</div>
<?php  endif; ?>
