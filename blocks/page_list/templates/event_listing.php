<?php  

defined('C5_EXECUTE') or die("Access Denied.");
$textHelper = Loader::helper("text");
$imgHelper = Loader::Helper('image');
	
if (count($cArray) > 0): 
	echo '<div class="event-listing">';
	
	for ($i = 0; $i < count($cArray); $i++ ):
		$cobj = $cArray[$i]; 
		$url = $nh->getLinkToCollection($cobj);
		$target = $cobj->getAttribute('nav_target');
		$title = $cobj->getCollectionName();
		if(!$controller->truncateSummaries):
			$summary = $cobj->getCollectionDescription();
		else:
			$summary = $textHelper->wordSafeShortText($cobj->getCollectionDescription(),$controller->truncateChars);
		endif;

		$start = $cobj->getAttribute('start_date');
		$end = $cobj->getAttribute('end_date');

		if (substr($start, 0, 10)==substr($end, 0, 10)):
			$date = 
					'<div class="date single">'.
						'<span class="month">'.date('M', strtotime($start)).'</span>'.
						'<span class="day">'.date('j', strtotime($start)).'</span>'.
					'</div>';

		else:
			$date = '<div class="date double">'.
						'<span>'.date('j. M', strtotime($start)).'</span>'.
						'<span>'.date('j. M', strtotime($end)).'</span>'.
					'</div>';
		endif;

		?>
		<a class="item clearfix" href="<?php  echo $url ?>">
			<?= $date; ?>
			<h2><?php  echo $title ?></h2>
			<div class="summary">
				<?= $summary ?>
			</div>
		</a>
		<?php
	endfor;

	if ($paginate && $num > 0 && is_object($pl) && ($summary = $pl->getSummary()) && $summary->pages > 1): 
		?><div id="pagination"><?php  echo $paginator->getPages(); ?></div><?php
	endif;

	echo '</div>';	
endif;
