<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$dh = Loader::helper('date'); //<--uncomment this line if displaying dates (see below)
$imgHelper = Loader::Helper('image');

?>

<div class="latest-news">
	<?php foreach ($pages as $page): 

		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$description = $page->getCollectionDescription();
		$description = $controller->truncateSummaries ? $th->shorten($description, $controller->truncateChars) : $description;
		$description = $th->entities($description);	
		$month = $page->getCollectionDatePublic('M');
		$day = $page->getCollectionDatePublic('d');
	?>
	<a class="item row nowrap" href="<?php  echo $url ?>">
		<div class="date-box span1">
			<div><em><strong>
				<span class="month"><?= $month ?></span>
				<span class="day"><?= $day ?></span>
			</strong></em></div>	
		</div>
		<div class="span3">
			<h2><?php  echo $title ?></h2>
			<div class="summary">
				<?= $description ?>
			</div>
		</div>
	</a>	
	<?php endforeach; ?>
</div>
