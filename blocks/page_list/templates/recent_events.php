<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$dh = Loader::helper('date'); //<--uncomment this line if displaying dates (see below)

$now = time();
$usedPages = array();
$futureEvents = 0;
foreach ($pages as $page):
	$ts = $page->getAttribute('start_date');
	if ($ts>$time) {
		$futureEvents++;
	}
	$usedPages[] = $page;
	if ($futureEvents>3) {
		array_shift($usedPages);
	}
endforeach;
$usedPages = array_slice($usedPages, 0, 2);



?>
<div class="recent-events">
<?php foreach ($usedPages as $page): 

	$title = $th->entities($page->getCollectionName());
	$url = $nh->getLinkToCollection($page);
	$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
	$target = empty($target) ? '_self' : $target;
	$description = $page->getCollectionDescription();
	$description = $controller->truncateSummaries ? $th->shorten($description, $controller->truncateChars) : $description;
	$description = $th->entities($description);	

	$start = $page->getAttribute('start_date');
	$end = $page->getAttribute('end_date');

	if (substr($start, 0, 10)==substr($end, 0, 10)):
		$date = 
				'<div class="date single">'.
					'<span class="day">'.date('j', strtotime($start)).'</span>'.
					'<span class="month">'.date('M', strtotime($start)).'</span>'.
				'</div>';

	else:
		$date = '<div class="date double">'.
					'<span>'.date('j. M', strtotime($start)).'</span>'.
					'<span>'.date('j. M', strtotime($end)).'</span>'.
				'</div>';
	endif;

?>
	<div class="event clearfix">
		<h3><a href="<?php  echo $url ?>" target="<?php  echo $target ?>"><?php  echo $title ?></a></h3>
		<?= $date; ?>
		<div class="desc">
			<?= $description ?>
		</div>
	</div>
<?php endforeach; ?>
</div>