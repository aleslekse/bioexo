<?php 

	defined('C5_EXECUTE') or die("Access Denied.");
	class PageListBlockController extends Concrete5_Controller_Block_PageList {
		protected $btCacheBlockRecord = false;
		protected $btCacheBlockOutput = false;
		protected $btCacheBlockOutputOnPost = false;
		protected $btCacheBlockOutputForRegisteredUsers = false;
		
		public function getPageList() {
			Loader::model('page_list');
			$db = Loader::db();
			$bID = $this->bID;
			if ($this->bID) {
				$q = "select num, cParentID, cThis, orderBy, ctID, displayAliases, rss from btPageList where bID = '$bID'";
				$r = $db->query($q);
				if ($r) {
					$row = $r->fetchRow();
				}
			} else {
				$row['num'] = $this->num;
				$row['cParentID'] = $this->cParentID;
				$row['cThis'] = $this->cThis;
				$row['orderBy'] = $this->orderBy;
				$row['ctID'] = $this->ctID;
				$row['rss'] = $this->rss;
				$row['displayAliases'] = $this->displayAliases;
			}
			

			$pl = new PageList();
			$pl->setNameSpace('b' . $this->bID);
			
			$cArray = array();

			switch($row['orderBy']) {
				case 'display_asc':
					$pl->sortByDisplayOrder();
					break;
				case 'display_desc':
					$pl->sortByDisplayOrderDescending();
					break;
				case 'chrono_asc':
					$pl->sortByPublicDate();
					break;
				case 'alpha_asc':
					$pl->sortByName();
					break;
				case 'alpha_desc':
					$pl->sortByNameDescending();
					break;
				default:
					$m = array();
					if (preg_match('/^attr_(.+?)_(asc|desc)/', $row['orderBy'], $m)) {
						$pl->sortBy($m[1], $m[2]);
					} else {
						$pl->sortByPublicDateDescending();	
					}
					break;
			}

			$num = (int) $row['num'];
			$pl->setItemsPerPage($num);			

			$c = Page::getCurrentPage();
			if (is_object($c)) {
				$this->cID = $c->getCollectionID();
			}
			
			Loader::model('attribute/categories/collection');
			if ($this->displayFeaturedOnly == 1) {
				$cak = CollectionAttributeKey::getByHandle('is_featured');
				if (is_object($cak)) {
					$pl->filterByIsFeatured(1);
				}
			}
			if (!$row['displayAliases']) {
				$pl->filterByIsAlias(0);
			}
			$pl->filter('cvName', '', '!=');			
		
			if ($row['ctID']) {
				$pl->filterByCollectionTypeID($row['ctID']);
			}
			
			$columns = $db->MetaColumns(CollectionAttributeKey::getIndexedSearchTable());
			if (isset($columns['AK_EXCLUDE_PAGE_LIST'])) {
				$pl->filter(false, '(ak_exclude_page_list = 0 or ak_exclude_page_list is null)');
			}
			
			if ( intval($row['cParentID']) != 0) {
				$cParentID = ($row['cThis']) ? $this->cID : $row['cParentID'];
				if ($this->includeAllDescendents) {
					$pl->filterByPath(Page::getByID($cParentID)->getCollectionPath());
				} else {
					$pl->filterByParentID($cParentID);
				}
			}

			Loader::model('attribute/categories/collection');
		  	$externalFilter = $this->getAllowedExternalFilter();
			foreach ($externalFilter as $attrHandle) {
				if (!empty($_GET[$attrHandle]) && ($attr = CollectionAttributeKey::getByHandle($attrHandle))) {
					$value = $_GET[$attrHandle];
					
					if ($attr->atHandle=='select') {
						$pl->filterBySelectAttribute($attrHandle, $value);
					} else {
						$pl->filterByAttribute($attrHandle, $value);
					}
				}
			}

			return $pl;
		}

		public function save($args) {
			parent::save($args);

			$db= Loader::db();
			$db->query('INSERT INTO btPageListEx (bID,externalFilter) VALUES(?,?) ON DUPLICATE KEY UPDATE externalFilter=?', array(
				$this->bID,
				$args['externalFilter'],
				$args['externalFilter'],
			));
		}

		public function getAllowedExternalFilter() {
			$db= Loader::db();
			$externalFilter = $db->GetOne('SELECT externalFilter FROM btPageListEx WHERE bID=?', array(
				$this->bID,				
			));

			return preg_split('/\s*,\s*/', $externalFilter, -1, PREG_SPLIT_NO_EMPTY);
		}

		public function edit() {
			parent::edit();

			$db= Loader::db();
			$externalFilter = $db->GetOne('SELECT externalFilter FROM btPageListEx WHERE bID=?', array(
				$this->bID,				
			));

			$this->set('externalFilter', $externalFilter);
		}

		public function fetchFilterValues($attribute) {
			Loader::model('attribute/categories/collection');
		  	$attr = CollectionAttributeKey::getByHandle($attribute);
			$db= Loader::db();

			$q = "select num, cParentID, cThis, orderBy, ctID, displayAliases, rss from btPageList where bID = '{$this->bID}'";
			$r = $db->query($q);
			if ($r) {
				$row = $r->fetchRow();
			}

		  	if (!$attr || !in_array($attr->atHandle, array('number', 'select'))) {
		  		return array();
		  	}


			
			$sql = <<<sql
select
	distinct(ia.ak_$attribute)
from
	CollectionVersions cv
inner join
	CollectionSearchIndexAttributes ia	
on
	cv.cID=ia.cID	
	
where
	cv.cvIsApproved=1
	and cv.ptID<>0
sql;

			if ($row['ctID']) {
				$sql .= " and cv.ctID=$row[ctID]";
			}

			$values = array();
			foreach ($db->GetAll($sql) as $row) {
				$v = preg_split('/[\r\n]+/', (string)$row["ak_$attribute"], -1, PREG_SPLIT_NO_EMPTY);
				$values = array_merge($values, $v);
			}

			return array_unique($values);
		}

		public function cacheBlockOutput() {
			$ar = $this->getAllowedExternalFilter();
			return empty($ar) && empty($this->paginate);
		}
	}
