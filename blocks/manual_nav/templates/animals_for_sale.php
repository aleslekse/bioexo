<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php

$imgHelper = Loader::Helper('image');
$nh = Loader::Helper('navigation');
$date = Loader::helper('date');

$page = Page::getByID(137);
$url = $nh->getLinkToCollection($page);

?>
<div class="animals-for-sale">
	<div class="mediabox">
	<ul class="tiles"><?php foreach ($links as $link):
		$page = Page::getByID($link->cID);

		$title = $page->getCollectionName();
		$gender = $page->getAttribute('gender')=='male'?'Moški':'Ženski';
		$birthday = $page->getAttribute('birthday'); 
		$available = $page->getAttribute('available');
		$desc = $page->getCollectionDescription();

	?><li>
		<a href="<?php  echo $url; ?>#a<?= $link->cID ?>">
			<?php $imgHelper->outputThumbnail($page->getAttribute('image'), 460, 355, $title, false, true); ?>
			<h2><?= $title; ?></h2>
			<dl>
				<dt>
					<?= $desc; ?>
				</dt>
				<dd>
					Starost: <?= $date->timeSince(strtotime($birthday)); ?>
				</dd>
				<dd>
					Spol: <?= $gender; ?>
				</dd>
				<dd>
					Na voljo: <?= $available; ?>
				</dd>
			</dl>
		</a>
	</li>
	<?php endforeach; ?></ul>
	</div>	
</div>
