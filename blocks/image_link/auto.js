ccmValidateBlockForm = function() {
	
	if ($('#field_1_image_fID-fm-value').val() == '' || $('#field_1_image_fID-fm-value').val() == 0) {
		ccm_addError('Missing required image: Image');
	}

	if ($('select[name=field_2_select_value]').val() == '' || $('select[name=field_2_select_value]').val() == 0) {
		ccm_addError('Missing required selection: Sticker');
	}


	return false;
}
