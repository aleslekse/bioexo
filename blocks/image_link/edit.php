<?php  defined('C5_EXECUTE') or die("Access Denied.");
$al = Loader::helper('concrete/asset_library');
$ps = Loader::helper('form/page_selector');
?>

<style type="text/css" media="screen">
	.ccm-block-field-group h2 { margin-bottom: 5px; }
	.ccm-block-field-group td { vertical-align: middle; }
</style>

<div class="ccm-block-field-group">
	<h2>Title</h2>
	<?php 
	echo $form->text('field_3_text_value', $field_3_text_value);
	?>
</div>

<div class="ccm-block-field-group">
	<h2>Image</h2>
	<?php  echo $al->image('field_1_image_fID', 'field_1_image_fID', 'Choose Image', $field_1_image); ?>

	<table border="0" cellspacing="3" cellpadding="0" style="width: 95%;">
		<tr>
			<td align="right" nowrap="nowrap"><label for="field_1_image_internalLinkCID">Link to Page:</label>&nbsp;</td>
			<td align="left" style="width: 100%;"><?php  echo $ps->selectPage('field_1_image_internalLinkCID', $field_1_image_internalLinkCID); ?></td>
		</tr>
	</table>
</div>

<div class="ccm-block-field-group">
	<h2>Sticker</h2>
	<?php 
	$options = array(
		'0' => '--Choose One--',
		'1' => 'aktualna_prireditev',
		'2'	=> 'aktualna_storitev',
		'3'	=> 'nasvet_meseca',
		'4'	=> 'novo_v_trgovini',
		'5'	=> 'pocitniske_dejavnosti',
		'6'	=> 'zival_meseca',
	);
	echo $form->select('field_2_select_value', $options, $field_2_select_value);
	?>
</div>



