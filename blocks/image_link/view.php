<?php  defined('C5_EXECUTE') or die("Access Denied.");
$nh = Loader::helper('navigation');

$stickers = array(
	'1' => 'aktualna_prireditev',
	'2'	=> 'aktualna_storitev',
	'3'	=> 'nasvet_meseca',
	'4'	=> 'novo_v_trgovini',
	'5'	=> 'pocitniske_dejavnosti',
	'6'	=> 'zival_meseca',
);

$stickerCls = '';
if (isset($stickers[$field_2_select_value])):
	$stickerCls = 'sticker_'.$stickers[$field_2_select_value];
endif;

if (!empty($field_1_image_internalLinkCID)):
	$link = $nh->getLinkToCollection(Page::getByID($field_1_image_internalLinkCID), true);
else:
	$link = '';	
endif;

?>

<a href="<?= $link ?>" class="image-link <?= $stickerCls ?>">
	<h2><span><?= $field_3_text_value ?></span></h2>
	<img src="<?php  echo $field_1_image->src; ?>" />
</a>

