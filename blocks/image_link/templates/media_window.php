<?php  defined('C5_EXECUTE') or die("Access Denied.");
$nh = Loader::helper('navigation');
$imgHelper = Loader::Helper('image');

$stickers = array(
	'1' => 'aktualna_prireditev',
	'2'	=> 'aktualna_storitev',
	'3'	=> 'nasvet_meseca',
	'4'	=> 'novo_v_trgovini',
	'5'	=> 'pocitniske_dejavnosti',
	'6'	=> 'zival_meseca',
);

$stickerCls = '';
if (isset($stickers[$field_2_select_value])):
	$stickerCls = 'sticker_'.$stickers[$field_2_select_value];
endif;

if (!empty($field_1_image_internalLinkCID)):
	$link = $nh->getLinkToCollection(Page::getByID($field_1_image_internalLinkCID), true);
else:
	$link = '';	
endif;
$image = $controller->get_image();

?>
<a href="<?= $link ?>" class="image-link <?= $stickerCls ?>">
	<h2><span><?= $field_3_text_value ?></span></h2>
	<?php $imgHelper->outputThumbnail($image, 1176, 391, $title, false, true); ?>
</a>

