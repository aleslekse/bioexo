<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$html = Loader::helper('html');
$imgHelper = Loader::helper('image');
$rel = "fancybox{$controller->bID}"; 
$c = Page::getCurrentPage();

if (!in_array($displayColumns, array(1,2,4))):
	$displayColumns = 2;
endif;

$span_width = 4/$displayColumns;

$img_width = $span_width*197;

?>

<div class="gallery">
<?php foreach (array_chunk($images, $displayColumns) as $chunk): ?>
	<div class="row">
		<?php  foreach ($chunk as $img): ?>
		<div class="span<?= $span_width ?>">
			<?php  if ($enableLightbox): ?>
				<a class="box" href="<?php  echo $img['full_src']; ?>" rel="<?php  echo $rel; ?>" title="<?php  echo $img['description']; ?>">
					<?php  echo $html->image($img['thumb_src'], $img['thumb_width'], $img['thumb_height'], array('alt' => $img['title']), true, true); ?>
				</a>
			<?php  else: ?>
				<span class="inner">
					<?php  echo $html->image($img['full_src'], $img_width, $img_width, array('alt' => $img['title']), true, true); ?>
				</span>
			<?php  endif; ?>
		</div>
		<?php endforeach; ?>
	</div>
<?php endforeach; ?>
</div>	


<?php  if (!$c->isEditMode() && $enableLightbox && count($images) > 0): /* fancybox init chokes if no applicable dom elements */ ?>
<script type="text/javascript">
	$(function(){
		document.body.addEventListener('touchstart', function(){});
		
		$(".gallery a").photoSwipe({
			autoStartSlideshow: true,
			backButtonHideEnabled:true,
			imageScaleMethod: 'fitNoUpscale',
			enableUIWebViewRepositionTimeout: true
			//,swipeTimeThreshold: 500
		});

	});
</script>
<?php  endif; ?>

