<?php
defined('C5_EXECUTE') or die("Access Denied.");
$ih = Loader::helper('image');
$html = Loader::helper('html');
$fileObj = $controller->getFileObject();

$full = $ih->getThumbnail($fileObj, 800, 600);
$thumb = $ih->getThumbnail($fileObj, 420, 420);


?><a class="box thumb" href="<?= $full->src ?>">
<?php 

echo $html->image($thumb->src, $thumb->thumb_width, $thumb->thumb_height, array('alt' => $thumb->title), true, true); 

?>
</a>
<script type="text/javascript">
	$(function(){
		document.body.addEventListener('touchstart', function(){});
		
		$(".thumb").photoSwipe({
			autoStartSlideshow: true,
			backButtonHideEnabled:true,
			imageScaleMethod: 'fitNoUpscale',
			enableUIWebViewRepositionTimeout: true
			//,swipeTimeThreshold: 500
		});

	});
</script>