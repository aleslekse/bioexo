<?php
die;

set_time_limit(0);

Loader::library("file/importer");
$fi = new FileImporter();

echo '<pre>';

$dogodkiSet = FileSet::createAndGetSet('Dogodki', FileSet::TYPE_PUBLIC);
$dogodkiTaxonomy = array(
	18=>'Delavnica',
	21=>'Izleti',
	20=>'Naravoslovni dan',
	19=>'Razstava',
	22=>'Rojstni dan',
	23=>'Sejmi',
);

$dogodki = array();
foreach (glob('old/dogodki/*') as $file) {
	$it = eval('return '.file_get_contents($file).';');
	$dogodki = array_merge($dogodki, $it);
}
$parentPage = Page::getByID(128);

//$Z = 2;
foreach ($dogodki as $dogodek) {
/*	$Z--;
	if ($Z<0) break;*/
	
	echo "Adding dogodek ".$dogodek['title']."\n";
	$taxonomyId = end($dogodek['taxonomy'][5]);
	
	$pt = CollectionType::getByHandle("event");
	$newPage = $parentPage->add($pt,array(
		'name' => $dogodek['title'],
		'cDescription'=>strip_tags($dogodek['teaser']),
		
	));
	$newPage->setAttribute('start_date', $dogodek['field_start'][0]['value']);
	$newPage->setAttribute('end_date', $dogodek['field_start'][0]['value2']);
	
	
	
	if ($dogodkiTaxonomy[$taxonomyId]) {
		$newPage->setAttribute('event_type', array($dogodkiTaxonomy[$taxonomyId]));
	}
	
	$newPage->update(array(
		'cDatePublic'=>date('Y-m-d H:i:s', $dogodek['created'])
	));
	
	
	if ($dogodek['field_image_cache'][0]) {
		$url = $dogodek['field_image_cache'][0]['node_export_file_url'];
		$dir = sys_get_temp_dir();
		$dl_file = "$dir/".basename($url);
		if (file_exists($dl_file)) {
			unlink($dl_file);	
		}
		file_put_contents($dl_file, file_get_contents($url));
		$file = $fi->import($dl_file);
		
		$dogodkiSet->addFileToSet($file->getFileID());
		
		$block = BlockType::getByHandle('image');
		$data = array(
			'fID'=>$file->getFileID(),
		);
		$newBlock = $newPage->addBlock($block, 'Main', $data);
		$newBlock->setCustomTemplate('thumb.php');
		
		$newPage->setAttribute('image', $file);
	}
	
	$block = BlockType::getByHandle('content');
	$data = array(
		'content' => $dogodek['body']
	);
	$newBlock = $newPage->addBlock($block, 'Main', $data);
	
	if ($dogodek['field_attachments'][0]) {
		$url = $dogodek['field_attachments'][0]['node_export_file_url'];
		$dir = sys_get_temp_dir();
		$dl_file = "$dir/".basename($url);
		if (file_exists($dl_file)) {
			unlink($dl_file);	
		}
		file_put_contents($dl_file, file_get_contents($url));
		$file = $fi->import($dl_file);
		
		$dogodkiSet->addFileToSet($file->getFileID());
		
		$block = BlockType::getByHandle('file');
		$data = array(
			'fID'=>$file->getFileID(),
		);
		$newBlock = $newPage->addBlock($block, 'Main', $data);
	}
	
	
	
}





$images = array();
foreach (glob('old/slike/*') as $file) {
	$it = eval('return '.file_get_contents($file).';');
	$images = array_merge($images, $it);
}

$usedGalleryTaxonomies = array();
foreach ($images as $image) {
	$taxonomyId = end($image['taxonomy'][4]);
	$usedGalleryTaxonomies[$taxonomyId] = 1;
}

$oldGalleryTaxonomies = array(
	34=>'Aquabeach sejem v Ceseni, september 2011',
	33=>'Bioexo v Arboretumu',
	31=>'Ekskurzije',
	16=>'Galerija 1',
	17=>'Galerija 2',
	26=>'Planet Tuš Celje',
	30=>'Planet Tuš Koper',
	27=>'Planet Tuš Kranj',
	29=>'Planet Tuš Maribor',
	28=>'Planet Tuš Novo mesto',
	36=>'Razne živalske',
	24=>'Razstave v Planetih Tuš',
	35=>'Terraplaza Budimpešta februar 2011',
);

$oldGalleryTaxonomies = array_intersect_key($oldGalleryTaxonomies, $usedGalleryTaxonomies);

$fileSets = array();
foreach ($oldGalleryTaxonomies as $taxonomyID=>$taxonomy) {
	$fileSets[$taxonomyID] = FileSet::createAndGetSet($taxonomy, FileSet::TYPE_PUBLIC);
}

foreach ($images as $image) {
	$url = $image['field_gallery_picture'][0]['node_export_file_url'];
	$taxonomyId = end($image['taxonomy'][4]);
	if (!isset($fileSets[$taxonomyId])) {
		echo "Skipping $url, set not found $taxonomyId\n";
		continue;
	}
	$fileSet = $fileSets[$taxonomyId];

	
	$dir = sys_get_temp_dir();
	$dl_file = "$dir/".basename($url);
	if (file_exists($dl_file)) {
		unlink($dl_file);	
	}
	file_put_contents($dl_file, file_get_contents($url));
	
	
	$file = $fi->import($dl_file);
	if (is_object($file)) {
		$fileSet->addFileToSet($file->getFileID());
		echo "$url added to set $taxonomyId\n";
	} else {
		echo "Failed adding $url to set $taxonomyId: code=$file\n";
	}
}

$parentPage = Page::getByID(143);
echo "Adding gallery pages\n";
foreach ($fileSets as $taxonomyID=>$fileSet) {
	$pt = CollectionType::getByHandle("gallery");
	$newPage = $parentPage->add($pt,array(
		'name' => $oldGalleryTaxonomies[$taxonomyID],
	));
	$block = BlockType::getByHandle('sortable_fancybox_gallery');
	$data = array(
		'enableLightbox' => 1,
		'fullWidth'=>800,
		'fullHeight'=>600,
		'displayColumns'=>4,
		'lightboxTransitionEffect'=>'fade',
		'lightboxTitlePosition'=>'outside',
		'thumbWidth'=>150,
		'thumbHeight'=>150,
		'fsID'=>$fileSet->getFileSetID()
	);
	$newBlock = $newPage->addBlock($block, 'Main', $data);
}
echo "Gallery pages added\n";

$novice = array();
foreach (glob('old/novice/*') as $file) {
	$it = eval('return '.file_get_contents($file).';');
	$novice = array_merge($novice, $it);
}

$noviceSet = FileSet::createAndGetSet('Novice', FileSet::TYPE_PUBLIC);
$parentPage = Page::getByID(140);
foreach ($novice as $novica) {
	echo "Adding novica ".$novica['title']."\n";
	
	$pt = CollectionType::getByHandle("right_sidebar");
	$newPage = $parentPage->add($pt,array(
		'name' => $novica['title'],
		'cDescription'=>strip_tags($novica['teaser'])
		
	));
	$newPage->update(array(
		'cDatePublic'=>date('Y-m-d H:i:s', $novica['created'])
	));
	
	if ($novica['field_news_picture']) {
		$url = $novica['field_news_picture'][0]['node_export_file_url'];
		$dir = sys_get_temp_dir();
		$dl_file = "$dir/".basename($url);
		if (file_exists($dl_file)) {
			unlink($dl_file);	
		}
		file_put_contents($dl_file, file_get_contents($url));
		
		$file = $fi->import($dl_file);
		if (is_object($file)) {
			$noviceSet->addFileToSet($file->getFileID());
			
			$block = BlockType::getByHandle('image');
			$data = array(
				'fID'=>$file->getFileID(),
			);
			$newBlock = $newPage->addBlock($block, 'Main', $data);
			$newBlock->setCustomTemplate('thumb.php');
		}
	}
	
	$block = BlockType::getByHandle('content');
	$data = array(
		'content' => $novica['body']
	);
	$newBlock = $newPage->addBlock($block, 'Main', $data);
}


echo '</pre>';