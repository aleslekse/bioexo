<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
	
	<article id="firstpage">
		<div class="row">
		<?php 

		$a = new Area('Main left');
		$a->setBlockWrapperStart('<div class="box">');
		$a->setBlockWrapperEnd('</div>');
		echo '<div class="span2">';
		$a->display($c);
		echo '</div>';
		
		$a = new Area('Main right');
		$a->setBlockWrapperStart('<div class="box">');
		$a->setBlockWrapperEnd('</div>');
		echo '<div class="span2">';
		$a->display($c);
		echo '</div>';
		
		?>
		</div>
	</article>
<?php  $this->inc('elements/footer.php'); ?>
