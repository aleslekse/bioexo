<?php

$media = '';
foreach (glob('images/media_sticker*') as $file) {
	$file = basename($file);
	preg_match('/^media_sticker_(.+?)\.png$/', $file, $m);
	$cls = $m[1];

	$media .= <<<CSS
.media-window .sticker_$cls h2 span:before {
	background-image:url(images/$file);
}

CSS;
}

echo "$media\n\n";


$media = '';
foreach (glob('images/sticker*') as $file) {
	$file = basename($file);
	preg_match('/^sticker_(.+?)\.png$/', $file, $m);
	$cls = $m[1];

	$media .= <<<CSS
.box .image-link.sticker_$cls:before {
	background-image:url(images/$file);
}

CSS;
}

echo "$media\n\n";


