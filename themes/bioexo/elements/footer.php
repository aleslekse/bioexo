<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<footer>

	<div class="row">
		<?php
			$a = new GlobalArea('Footer');
			$a->setBlockWrapperStart('<div class="span1"><div>', true);
			$a->setBlockWrapperEnd('</div></div>');
			$a->display($c);
		?>
	</div> 
</footer>
</div>
<?php  Loader::element('footer_required'); ?>
</body>
</html>