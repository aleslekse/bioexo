<?php

$searchPage = Page::getByID(156);
$nh = Loader::helper('navigation');

$this->addHeaderItem($html->javascript('simple-inheritance.min.js'));
$this->addHeaderItem($html->javascript('klass.min.js'));
$this->addHeaderItem($html->javascript('code.photoswipe.jquery-3.0.5.min.js'));
$this->addHeaderItem($html->javascript('main.js'));

?><?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="<?php echo LANGUAGE?>" xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php  Loader::element('header_required'); ?>
	
<!-- Site Header Content //-->
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('css/gridism.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('css/normalize.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('css/photoswipe.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('css/main.css')?>" />
<? /*
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />
*/ ?>

</head>
<body<?php if ($c->isEditMode()) echo ' class="edit-mode"';  ?>>
	<div id="headerSpacer"></div>
	<div id="wrapper">
		<header>
			<div class="top">
				<a href="<?= View::url('/') ?>" class="home">Bioexo</a>
				<ul class="social hor clearfix">
					<li>
						<a class="fb" href="https://sl-si.facebook.com/pages/Dru%C5%A1tvo-ljubiteljev-eksoti%C4%8Dnih-%C5%BEivali-Bioexo/204188296320819">Facebook</a>
					</li>	
					<li>
						<a class="tw" href="https://twitter.com/BIOEXO1">Twitter</a>
					</li>
					<li>
						<a class="yt" href="https://www.youtube.com/channel/UCkAKd7BgMXozh6nCeIeE93g">Youtube</a>
					</li>
				</ul>
				<form class="search" action="<?php echo $nh->getCollectionURL($searchPage); ?>">
					<input name="search_paths[]" type="hidden" value="" />
					<input type="text" name="query" placeholder="Išči ..." />
					<button type="submit"></button>
				</form>
			</div>	

			<?php 			
			echo '<div class="mediabox media-window"><ul class="tiles">';
			$ah = new GlobalArea('Media window');
			$ah->setBlockLimit(3);
			$ah->setBlockWrapperStart('<li>');
			$ah->setBlockWrapperEnd('</li>');
			$ah->setCustomTemplate('image_link', 'media_window.php');
			$ah->display($c);
			echo '</ul></div>';
			
			$ah = new GlobalArea('Navigation');
			$ah->setBlockLimit(1);
			$ah->display($c);			
			?>	
			
		</header>			