<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
	<article>
		<div class="row">
			<div class="span3">
				<h1><?= $c->getCollectionName(); ?></h1>
				<?php 

				$a = new Area('Main');
				$a->setCustomTemplate('file', 'marker_type.php');
				$a->display($c);

				?>
			</div>
			<div class="span1 sidebar">
				<?php

				$a = new GlobalArea('Sidebar');
				$a->setBlockWrapperStart('<div class="box">');
				$a->setBlockWrapperEnd('</div>');
				$a->display($c);

				?>	
			</div>
		</div>
	</article>
<?php  $this->inc('elements/footer.php'); ?>
