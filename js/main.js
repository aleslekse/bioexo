$(function() {
	var editMode = $(document.body).hasClass('edit-mode');
	
	$('.mediabox').each(function() {
		var ul = $(this).find('.tiles'),
			size = ul.find('li').size(),
			current = 0,
			show,
			auto,
			delay,
			clear,
			timer = null,
			nav = $('<ul>')
		;
		
		if (editMode) {
			return;
		}
		
		show = function(delta) {
			var li = nav.find('li');
			
			current+=delta||1;
			
			if (current===size+1) {
				current = 1;
				ul.css('left', 0);
			} else if (current==-1) {
				current = size;
			}
			
			ul.animate({
				left:(-current*100)+'%'
			});
			
			li.removeClass('current');
			$(li.get(current===size?0:current)).addClass('current');
		};
		
		delay = function() {
			timer = setTimeout(auto, 2000);
		}
		
		clear = function() {
			if (timer!==null) {
				clearTimeout(timer);
				timer = null;
			}
		}
		
		auto = function() {
			show();
			clear();
			delay();
		}
		
		// append navigation
		nav.addClass('nav');
		ul.find('li').each(function(index) {
			nav.append($('<li>')
				.addClass(index==0?'current':'')
				.data('index', index)
			);
		});
		$(this).append(nav);
		
		// clone first card
		ul.append(ul.find('li:first').clone(true));
		
		// navigation events
		nav.on('click', 'li', function() {
			var index = $(this).data('index');
			
			clear();
			show(index-current);
		});
		
		$(this).hover(function() {
			clear();
		}, function() {
			delay();
		});
		
		delay();
	});

})